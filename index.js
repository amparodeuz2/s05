// Mathematical Operations (-, *, /, %)
//Subtraction
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3);//0.5 results in proper
//mathemetical operation
console.log(num3-num4);//5 results in proper 
//mathematical operation
console.log(numString1-num2);//-1 results in proper
//mathematical operation because the string was forced
//to become a number
console.log(numString2-num2);//0 
console.log(numString1-numString2);//-1 In subtraction
//numeric strings will not concatenate and instead
//will be forcibly changed its type and subtract properly
let sample2 = "Juan Dela Cruz";
console.log(sample2-numString1);//NaN - results in not a number. WHen trying to perform a subtraction between alphanumeric string and numeric string, the result is NaN

//Multiplication
console.log(num1*num2);//30
console.log(numString1*num1);//25
console.log(numString1*numString2)//30
console.log(sample2 * 5);

let product = num1 * num2;
let product2 = numString1 * num1;
let product3 = numString1 * numString2;
console.log(product);

//Division
console.log(product/num2);//5
console.log(product2/5);//5
console.log(numString2/numString1);//1.2
console.log(numString2 % numString1);//1

//Division or multiplication by 0
console.log(product2*0);//0
console.log(product3/0);//infinity
//Division by 0 is not accurately and should not be done, it results to infinity

//% modulo - remainder of division operation
console.log(product2 % num2);//product2 divided by num2(25/6)= remainder =1
console.log(product3 % product2);//remainder = 5
console.log(num1%num2);//5
console.log(num1 % num1);//0

//Boolean(true or false)
/*
Boolean is usually used for logic operations or if else condition
when creating a variable which will contian a boolean, the variable
name is usually a yes or no question
*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;

//You can also concatenate strings + boolean
console.log("Is she married? " + isMarried);
console.log("Is he the MVP? "+ isMVP);
console.log('Is he the current admin? '  + isAdmin);

//Arrays
/*
Arrays are a special kind of data type to store multiple values
- Can actually store data with different types BUTas the best practice, arrays are used to contain multiple values with the same data type
- values in array are separated by commas

an array is created with an array literal []
*/
//Syntax;
	//let/constant arrayName = [elementA, elementB, ...]
let array1 = ["Luffy", "Zoro", "Sanji", "Jinbei"];
console.log(array1);
let array2 = ["One Punch Man", true, 500, "Saitama"]
console.log(array2);
let grades = [98.7, 92.1, 90.2, 94.6]
console.log(grades);

//Array are better though of as group data

//Objects
/*
Objects are another special kind of data type used to mimic the real world
- used to create complex data that contain pieces of information that are relevant to each other
- objects are created with object literals {}
- each data/value are paired with a key
-each field is called a property
-each field is separated with comma

syntax:
let/const objectName = {
	propertyA: value,
	propertB: value,
}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact:["first number", "second number"],
	address: {
		houseNum : '345',
		street: "Diamond",
		city: "Manila",
	}
};
console.log(person);

/*
Mini Activity
*/

let sleepingWithSirens = ["Kellin Quinn", "Justin Hills","Gabe Barham","Jack Fowler","..."]
console.log(sleepingWithSirens)

let person1 = {
	firstName: "Deuz",
	lastName: "Amparo",
	isDeveloper: false,
	hasPortfolio: false,
	age: 20,
	contact:["+639260843***", "+639605583***"],
	address: {
		houseNum : 'blk 5 lot 7 ',
		street: "San Miguel Avenue",
		city: "Bacoor",
}};
console.log(person1);

//Undefined vs Null
//null is a explicit absence of data/value. this is done to project that a variable contains nothing over undefined
//Undefined - merely means there is no data in the variable because the variable has not been assigned an initial value
let sampleNull = null;

let myNUmber = 0;
let myString = " ";
//using null to compare a zero value and an empty string is much better for readability

let sampleUndefined;
console.log(sampleNull);
console.log(sampleUndefined);

let person2 = {
	name: "Peter",
	age: 35,

};
//person2 dos exist, however, the property isAdmin does not
console.log(person2.isAdmin);

//Functions
/*
In javascript, are lines/blocks of code that tell our device/application to perform a certain task when called/invoked
- are reusable pcs of codes with unstructions wihich used over and over again just as ling as we can call/invoke them

sytax:
function functionName(){
	code block
	-the block of code that will be executed once the function has been run/called/invoked
}
*/

//Declaring a function
function printName(){
	console.log("My name is Deuz");
};

//Invoking or calling a function - functionName()
printName();

function showSum(){
	console.log(25+6);
};
showSum();
//Note: Do not create a function with a same name

/*
parameters and arguments
	"Name" is called a parameter
*/

function printName1(name){
	console.log(`My name is ${name}`);
};

printName1("Amparo");

function displayNum(number){
	alert(number);
};
displayNum(5000);

/*
MINI ACTIVITY
*/
function message(language){
	console.log(`${language} is fun`);
};
message('JavaScript');
message('C++');

//Multiple parameters and arguments
function displayFullName(fn, mi, lm, age){
	console.log(`${fn} ${mi} ${lm} ${age}`);
};

displayFullName("Deuz", "B", "Amparo", 20);

//return keyword
//statement that allows the output of a function to be passed to the line/block of code that invoked/called the function
//any line/block of code that comes after the return statement is ignored becuase it ends the function execution

function createFullName(fn, mn, ln){
//return keyword is used so that a function may return a value
	return `${fn} ${mn} ${ln}`
	console.log("I will no longer run because the function's value has been returned")
};

createFullName("Deuz Bilon Amparo");
